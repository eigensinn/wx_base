# wx_base

## Приложение для установки и мониторинга тока/напряжения в реальном времени для источников питания, поддерживающих SCPI

### Основные особенности

  * приложение основано на wxPython 4.0;
  * для создания интерфейса использовался [wxFormBuilder](https://github.com/wxFormBuilder/wxFormBuilder/releases);
  * т.к требуется отдельный поток для сбора данных, используется threading;
  * т.к. требуется асинхронно собирать данные по току и напряжению, используется asyncio;
  * для передачи сообщений между потоком сбора данных и основным потоком используется [Pypubsub](https://pypi.org/project/Pypubsub/);
  * Приложение тестировалось с источником питания [BK Precision XLN3640](https://www.bkprecision.com/products/power-supplies/XLN3640-36v-40a-144kw-programmable-dc-power-supply.html)
  * можно проверить работоспособность без источника питания с генератором случайных чисел;
  * раскомментируете код в ```after_run```, если хотите работать с источником питания.
  
### Лицензия

[MIT License](https://bitbucket.org/eigensinn/wx_base/src/master/LICENSE)

