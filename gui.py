# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Jul 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc


###########################################################################
## Class MyFrame1
###########################################################################

class MyFrame1(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                          size=wx.Size(945, 802), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        self.bSizer1 = wx.BoxSizer(wx.VERTICAL)

        self.m_panel1 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.fgSizer1 = wx.FlexGridSizer(0, 2, 0, 0)
        self.fgSizer1.SetFlexibleDirection(wx.BOTH)
        self.fgSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_panel1.SetSizer(self.fgSizer1)
        self.m_panel1.Layout()
        self.fgSizer1.Fit(self.m_panel1)
        self.bSizer1.Add(self.m_panel1, 1, wx.EXPAND | wx.ALL, 5)

        gSizer1 = wx.GridSizer(0, 2, 0, 0)

        self.m_notebook1 = wx.Notebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_panel2 = wx.Panel(self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        gSizer2 = wx.GridSizer(0, 2, 0, 0)

        self.txtctrl_port = wx.TextCtrl(self.m_panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer2.Add(self.txtctrl_port, 0, wx.ALL, 5)

        ch_baudChoices = [u"4800", u"9600", u"19200", u"38400", u"57600"]
        self.ch_baud = wx.Choice(self.m_panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, ch_baudChoices, 0)
        self.ch_baud.SetSelection(4)
        gSizer2.Add(self.ch_baud, 0, wx.ALL, 5)

        self.m_panel2.SetSizer(gSizer2)
        self.m_panel2.Layout()
        gSizer2.Fit(self.m_panel2)
        self.m_notebook1.AddPage(self.m_panel2, u"Настройки", False)
        self.m_panel3 = wx.Panel(self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        gSizer3 = wx.GridSizer(0, 3, 0, 0)

        self.m_button4 = wx.Button(self.m_panel3, wx.ID_ANY, u"*IDN?\n", wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer3.Add(self.m_button4, 0, wx.ALL, 5)

        self.m_staticText1 = wx.StaticText(self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText1.Wrap(-1)

        gSizer3.Add(self.m_staticText1, 0, wx.ALL, 5)

        self.txtctrl_idn = wx.TextCtrl(self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer3.Add(self.txtctrl_idn, 0, wx.ALL, 5)

        self.btn_fetc_curr = wx.Button(self.m_panel3, wx.ID_ANY, u"FETC:CURR ON", wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer3.Add(self.btn_fetc_curr, 0, wx.ALL, 5)

        self.btn_fetc_curr_off = wx.Button(self.m_panel3, wx.ID_ANY, u"FETC:CURR OFF", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        gSizer3.Add(self.btn_fetc_curr_off, 0, wx.ALL, 5)

        self.txtctrl_fc = wx.TextCtrl(self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer3.Add(self.txtctrl_fc, 0, wx.ALL, 5)

        self.btn_fetc_volt = wx.Button(self.m_panel3, wx.ID_ANY, u"FETC:VOLT ON", wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer3.Add(self.btn_fetc_volt, 0, wx.ALL, 5)

        self.btn_fetc_volt_off = wx.Button(self.m_panel3, wx.ID_ANY, u"FETC:VOLT OFF", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        gSizer3.Add(self.btn_fetc_volt_off, 0, wx.ALL, 5)

        self.txtctrl_fv = wx.TextCtrl(self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer3.Add(self.txtctrl_fv, 0, wx.ALL, 5)

        self.m_panel3.SetSizer(gSizer3)
        self.m_panel3.Layout()
        gSizer3.Fit(self.m_panel3)
        self.m_notebook1.AddPage(self.m_panel3, u"Управление", True)

        gSizer1.Add(self.m_notebook1, 1, wx.EXPAND | wx.ALL, 5)

        self.txtctrl_console = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.txtctrl_console.SetMinSize(wx.Size(400, 180))

        gSizer1.Add(self.txtctrl_console, 0, wx.ALL, 5)

        self.bSizer1.Add(gSizer1, 1, wx.EXPAND, 5)

        self.SetSizer(self.bSizer1)
        self.Layout()
        self.m_statusBar1 = self.CreateStatusBar(1, wx.STB_SIZEGRIP, wx.ID_ANY)
        self.m_menubar1 = wx.MenuBar(0)
        self.m_menu1 = wx.Menu()
        self.m_menuItem1 = wx.MenuItem(self.m_menu1, wx.ID_ANY, u"О программе", wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu1.Append(self.m_menuItem1)

        self.menuItem_exit = wx.MenuItem(self.m_menu1, wx.ID_ANY, u"Выйти", wx.EmptyString, wx.ITEM_NORMAL)
        self.m_menu1.Append(self.menuItem_exit)

        self.m_menubar1.Append(self.m_menu1, u"Файл")

        self.SetMenuBar(self.m_menubar1)

        self.Centre(wx.BOTH)

        # Connect Events
        self.m_button4.Bind(wx.EVT_LEFT_DOWN, self.idn)
        self.btn_fetc_curr.Bind(wx.EVT_LEFT_DOWN, self.fetc_curr)
        self.btn_fetc_curr_off.Bind(wx.EVT_LEFT_DOWN, self.fetc_curr_off)
        self.btn_fetc_volt.Bind(wx.EVT_LEFT_DOWN, self.fetc_volt)
        self.btn_fetc_volt_off.Bind(wx.EVT_LEFT_DOWN, self.fetc_volt_off)
        self.Bind(wx.EVT_MENU, self.exit_app, id=self.menuItem_exit.GetId())

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def idn(self, event):
        event.Skip()

    def fetc_curr(self, event):
        event.Skip()

    def fetc_curr_off(self, event):
        event.Skip()

    def fetc_volt(self, event):
        event.Skip()

    def fetc_volt_off(self, event):
        event.Skip()

    def exit_app(self, event):
        event.Skip()
