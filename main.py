from time import sleep
from serial import Serial
from serial.serialutil import SerialException
from os import path
from json import dump, load
import sys
from threading import Thread, Timer
from pubsub import pub
import random
import asyncio

# importing wx files
import wx
import wx.xrc

# import the newly created GUI file
import gui

import matplotlib
matplotlib.use('WXAgg')

from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure

home = path.abspath(path.dirname(sys.argv[0])) + "/"
CONFIGFILE = home + 'tasklist.json'


class DisplayThread(Thread):
    def __init__(self):
        Thread.__init__(self)

        #self.msg = msg
        self.state_fv = False
        self.state_fc = False
        self.state = True
        self.loop = asyncio.new_event_loop()

    def after_run(self):
        while self.state is True:
            if self.state_fv is True:
                pub.sendMessage("update", message=str(random.random()), arg2=frame.txtctrl_fv)
                # res = self.communication(b'FETC:VOLT?\n')
                # pub.sendMessage("update", message=res, arg2=frame.txtctrl_fv)
                sleep(1)
            if self.state_fc is True:
                pub.sendMessage("update", message=str(random.random()), arg2=frame.txtctrl_fc)
                # res = self.communication(b'FETC:CURR?\n')
                # pub.sendMessage("update", message=res, arg2=frame.txtctrl_fc)
                sleep(1)

    def communication(self, message):
        ser = Serial(port='COM' + frame.txtctrl_port.GetLineText(0),
                     baudrate=int(frame.ch_baud.GetString(frame.ch_baud.GetSelection())),
                     bytesize=8, parity='N', stopbits=1, timeout=0.1)
        try:
            ser.open()
        except SerialException as exc:
            pass
        finally:
            if ser.isOpen():
                ser.reset_input_buffer()
                ser.reset_output_buffer()
                sleep(0.05)
                ser.write(message)
                res = ser.readline()
                ser.close()
                return res.decode('utf-8').replace('\r\n', '')

    def run(self):
        asyncio.set_event_loop(self.loop)
        self.loop.run_forever()

    def stop(self):
        self.loop.call_soon_threadsafe(self.loop.stop)


class MyFrameSub(gui.MyFrame1):
    def __init__(self, parent):
        gui.MyFrame1.__init__(self, parent)
        self.axes = None
        self.createPlot()
        self.add_toolbar()
        self.load_settings()
        self.dt1, self.dt2 = None, None
        self.dots_fv = [[], []]
        self.dots_fc = [[], []]
        self.counter = 0

        pub.subscribe(self.display_value, "update")

        self.t = DisplayThread()
        # Stop thread after close application
        self.t.daemon = True
        self.t.start()

    def draw(self, message, dots, color):
        if len(dots[0]) > 10:
            del dots[0][0]
            del dots[1][0]
        dots[1].append(float(message))
        dots[0].append(self.counter)
        if dots == self.dots_fv:
            self.axes[0].clear()
            self.axes[0].plot(dots[0], dots[1], color)
            self.canvases[0].draw()
        if dots == self.dots_fc:
            self.axes[1].clear()
            self.axes[1].plot(dots[0], dots[1], color)
            self.canvases[1].draw()
        self.counter += 1

    def display_value(self, message, arg2=None):
        arg2.Clear()
        arg2.write(message)
        if arg2 == self.txtctrl_fv:
            self.draw(message, self.dots_fv, 'C0')
        elif arg2 == self.txtctrl_fc:
            self.draw(message, self.dots_fc, 'C1')

    def createPlot(self):
        self.figs = [Figure() for i in range(2)]
        self.canvases = [FigureCanvas(self.m_panel1, wx.ID_ANY, self.figs[i]) for i in range(2)]
        self.axes = [self.figs[0].add_subplot(111), self.figs[1].add_subplot(111)]
        self.axes[0].set_xlabel('Время, с', fontsize=8)
        self.axes[0].set_ylabel('U(V)', fontsize=8)
        self.axes[1].set_xlabel('Время, с', fontsize=8)
        self.axes[1].set_ylabel('I(A)', fontsize=8)
        self.fgSizer1.Add(self.canvases[0], 0, wx.LEFT | wx.TOP | wx.EXPAND)
        self.fgSizer1.Add(self.canvases[1], 1, wx.LEFT | wx.TOP | wx.EXPAND)

    def add_toolbar(self):
        self.toolbars = [NavigationToolbar2Wx(self.canvases[i]) for i in range(2)]
        self.toolbars[0].Realize()
        self.toolbars[1].Realize()
        self.fgSizer1.Add(self.toolbars[0], 2, wx.LEFT | wx.EXPAND)
        self.fgSizer1.Add(self.toolbars[1], 3, wx.LEFT | wx.EXPAND)
        self.toolbars[0].update()
        self.toolbars[1].update()

    def save_settings(self):
        settings = {'port': self.txtctrl_port.GetLineText(0),
                     'baud': self.ch_baud.GetSelection()}
        data = {
            'settings': settings
        }
        with open(CONFIGFILE, 'w') as f:
            dump(data, f)

    def load_settings(self):
        if path.exists(CONFIGFILE):
            with open(CONFIGFILE, 'r') as f:
                data = load(f)
            port = data['settings']['port']
            baud = data['settings']['baud']
            self.txtctrl_port.write(port)
            self.ch_baud.SetSelection(baud)

    def exit_app(self, event):
        self.t.state = False
        self.save_settings()
        self.Close()

    def idn(self, event):
        self.txtctrl_idn.Clear()
        res = self.communication(b'*IDN?\n')
        self.txtctrl_idn.write(res)

    def fetc_volt(self, event):
        self.t.state_fv = True
        self.t.loop.call_soon_threadsafe(self.t.after_run)


    def fetc_curr(self, event):
        self.t.state_fc = True
        self.t.loop.call_soon_threadsafe(self.t.after_run)

    def fetc_volt_off(self, event):
        self.txtctrl_fv.Clear()
        self.t.state_fv = False
        self.t.stop()

    def fetc_curr_off(self, event):
        self.txtctrl_fc.Clear()
        self.t.state_fc = False
        self.t.stop()

    def communication(self, message):
        ser = Serial(port='COM' + frame.txtctrl_port.GetLineText(0),
                     baudrate=int(frame.ch_baud.GetString(frame.ch_baud.GetSelection())),
                     bytesize=8, parity='N', stopbits=1, timeout=0.1)
        try:
            ser.open()
        except SerialException as exc:
            pass
        finally:
            if ser.isOpen():
                ser.reset_input_buffer()
                ser.reset_output_buffer()
                sleep(0.05)
                ser.write(message)
                res = ser.readline()
                ser.close()
                return res.decode('utf-8').replace('\r\n', '')


if __name__ == '__main__':
    # mandatory in wx, create an app, False stands for not deteriction stdin/stdout
    # refer manual for details
    app = wx.App(False)

    # create an object of CalcFrame
    frame = MyFrameSub(None)
    # show the frame
    frame.Show(True)
    # start the applications
    app.MainLoop()
